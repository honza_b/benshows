controllers.controller('ScheduleCtrl', ['$scope','$http','sID','lsService',
    function ($scope, $http, sID, lsService) {
        
        $scope.init = function() {
            $scope.showSpinner = true;
            $scope.showRefresh = false;
            
            $http({
                method: 'get',
                url: 'http://services.tvrage.com/feeds/fullschedule.php',
                cache: true,
                params: {
                    country: 'US',
                    key: '7A9bASGE4B4V4uPxp05r'
                }
            }).success(function(data){

                var parser = new X2JS();
                var jsonData = parser.xml_str2json(data);

                $scope.showSpinner = false;

                $scope.days = jsonData.schedule.DAY.slice(0,7);
                $scope.isObject = angular.isObject;
                $scope.isArray = angular.isArray;

            }).error(function(){
                alert('An error ocured');
                $scope.showSpinner = false;
                $scope.showRefresh = true;
            });
        };
        
        $scope.init();
        
        $scope.setSID = function(sid) {
            sID.data.sid = sid;
        };
        
        $scope.inArray = function(sid) {
            var fetchedIDs = JSON.parse(lsService.getMyShows("showsID"));
            
            if( fetchedIDs !== null) {
                if( fetchedIDs.indexOf(sid) === -1 ) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        };
    }
]);