services.value('version', '0.1');

services.factory('sID', function(){
    return {
        data: {
            sid: ''
        }
    };
});

services.service('lsService', function () {
    return {
        getMyShows: function(lsName) {
            return window.localStorage.getItem(lsName);
        },
        setMyShows: function(lsName,lsValue) {
            window.localStorage.setItem(lsName,lsValue);
        }
    }
});