'use strict';

// Declare chat level module which depends on views, and components
angular.module('BenShowsApp', [
    'ngRoute',
    'ngResource',
    'mobile-angular-ui',
    'BenShowsApp.filters',
    'BenShowsApp.services',
    'BenShowsApp.directives',
    'BenShowsApp.controllers'
]).
config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/schedule', {
                templateUrl: 'partials/schedule.html',
                controller: 'ScheduleCtrl'
            })
            .when('/info', {
                templateUrl: 'partials/info.html',
                controller: 'InfoCtrl'
            })
            .when('/cal', {
                templateUrl: 'partials/shows.html',
                controller: 'ShowsCtrl'
            })
            .when('/search', {
                templateUrl: 'partials/search.html',
                controller: 'SearchCtrl'
            })
            .otherwise({
                templateUrl: 'partials/main.html'
            });
}]);

//Initialize individual modules
var services = angular.module('BenShowsApp.services', []);
var factories = angular.module('BenShowsApp.factories', []);
var controllers = angular.module('BenShowsApp.controllers', []);
var filters = angular.module('BenShowsApp.filters', []);
var directives = angular.module('BenShowsApp.directives', []);