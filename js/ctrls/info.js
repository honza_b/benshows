controllers.controller('InfoCtrl', ['$scope','$http','sID','lsService',
    function ($scope, $http, sID, lsService) {
        
        var sid = sID.data.sid;
        
        $scope.isArray = angular.isArray;
        
        if( sid !== '' ) {
            $scope.init = function() {
                $scope.showSpinner = true;
                $scope.showRefresh = false;

                $http({
                    method: 'get',
                    url: 'http://services.tvrage.com/feeds/full_show_info.php',
                    cache: true,
                    params: {
                        sid: sid,
                        key: '7A9bASGE4B4V4uPxp05r'
                    }
                }).success(function(data){

                    var parser = new X2JS();
                    var jsonData = parser.xml_str2json(data);

                    $scope.showSpinner = false;

                    $scope.info = jsonData["Show"];

                }).error(function(){
                    alert('An error ocured');
                    $scope.showSpinner = false;
                    $scope.showRefresh = true;
                });
            }
            
            $scope.init();
        }
        
        $scope.addShow = function(sid,title,image) {
            var fetchedShows = lsService.getMyShows("myShows");
            var myShows = '{"shows":[]}';
            var fetchedIDs = lsService.getMyShows("showsID");
            var showsID = []
            
            if(typeof fetchedShows !== 'undefined' && fetchedShows !== null ) {
                myShows = JSON.parse(fetchedShows);
            } else {
                myShows = JSON.parse(myShows);
            }
            
            if(typeof fetchedIDs !== 'undefined' && fetchedIDs !== null ) {
                showsID = JSON.parse(fetchedIDs);
            }
            
            showsID.push(sid);
            
            myShows['shows'].push({"sid":sid,"title":title,"image":image});

            lsService.setMyShows("myShows",JSON.stringify(myShows));
            lsService.setMyShows("showsID",JSON.stringify(showsID));
        };
        
        $scope.removeShow = function(sid) {
            var fetchedIDs = JSON.parse(lsService.getMyShows("showsID"));
            var fetchedShows = JSON.parse(lsService.getMyShows("myShows"));
            
            var i = fetchedIDs.indexOf(sid);
            var shows = fetchedShows.shows;
            
            if(i !== -1) {
                fetchedIDs.splice(i,1);
                fetchedShows.shows.splice(i,1);
            }
            
            lsService.setMyShows("myShows",JSON.stringify(fetchedShows));
            lsService.setMyShows("showsID",JSON.stringify(fetchedIDs));
        };
        
        $scope.inArray = function(sid) {
            var fetchedIDs = JSON.parse(lsService.getMyShows("showsID"));
            
            if( fetchedIDs !== null) {
                if( fetchedIDs.indexOf(sid) === -1 ) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        };
    }
]);