controllers.controller('ShowsCtrl', ['$scope','$http','sID','lsService',
    function ($scope, $http, sID, lsService) {
        
        sendListToScope();
        
        $scope.removeShowFromList = function(sid) {
            var fetchedIDs = JSON.parse(lsService.getMyShows("showsID"));
            var fetchedShows = JSON.parse(lsService.getMyShows("myShows"));
            
            var i = fetchedIDs.indexOf(sid);
            var shows = fetchedShows.shows;
            
            if(i !== -1) {
                fetchedIDs.splice(i,1);
                fetchedShows.shows.splice(i,1);
            }
            
            lsService.setMyShows("myShows",JSON.stringify(fetchedShows));
            lsService.setMyShows("showsID",JSON.stringify(fetchedIDs));
            
            sendListToScope();
        };
        
        $scope.setSID = function(sid) {
            sID.data.sid = sid;
        };
        
        function sendListToScope() {
            var fetchedShows = JSON.parse(lsService.getMyShows("myShows"));
            $scope.shows = sortByKey(fetchedShows.shows,'title');
        };
        
        function sortByKey(array, key) {
            return array.sort(function(a, b) {
                var x = a[key]; var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        }
    }
]);