controllers.controller('SearchCtrl', ['$scope','$http','sID','lsService',
    function ($scope, $http, sID, lsService) {
        
        var sid = sID.data.sid;
        
        $scope.isArray = angular.isArray;
        
        $scope.search = function() {
            $scope.showSpinner = true;
		
            if( $scope.keywords !== '' ) {
                $http({
                    method: 'get',
                    url: 'http://services.tvrage.com/feeds/search.php',
                    cache: true,
                    params: {
                        show: $scope.keywords,
                        key: '7A9bASGE4B4V4uPxp05r'
                    }
                }).success(function(data){

                    var parser = new X2JS();
                    var jsonData = parser.xml_str2json(data);

                    $scope.showSpinner = false;

                    $scope.results = jsonData.Results.show;

                }).error(function(){
                    alert('An error ocured');
                    $scope.showSpinner = false;
                });
            }
        };
        
        $scope.setSID = function(sid) {
            sID.data.sid = sid;
        };
    }
]);